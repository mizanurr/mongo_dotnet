# MongoDB Cheatsheet

## Basic Operations

1. **Connect to MongoDB:**
    ```bash
    mongo
    ```

2. **Show All Databases:**
    ```bash
    show databases
    ```

3. **Switch Database:**
    ```bash
    use <database_name>
    ```

4. **Show Collections:**
    ```bash
    show collections
    ```

5. **Insert Document:**
    ```javascript
    db.collection_name.insert({ key: "value" })
    ```

6. **Query Documents:**
    ```javascript
    db.collection_name.find({ key: "value" })
    ```

7. **Update Document:**
    ```javascript
    db.collection_name.update({ key: "old_value" }, { $set: { key: "new_value" } })
    ```

8. **Delete Document:**
    ```javascript
    db.collection_name.remove({ key: "value" })
    ```

9. **Drop Collection:**
    ```javascript
    db.collection_name.drop()
    ```

## Query Operators

1. **Equality:**
    ```javascript
    { key: "value" }
    ```

2. **Greater Than:**
    ```javascript
    { key: { $gt: value } }
    ```

3. **Less Than:**
    ```javascript
    { key: { $lt: value } }
    ```

4. **Logical AND:**
    ```javascript
    { $and: [ { key1: "value1" }, { key2: "value2" } ] }
    ```

5. **Logical OR:**
    ```javascript
    { $or: [ { key1: "value1" }, { key2: "value2" } ] }
    ```

## Indexing

1. **Create Index:**
    ```javascript
    db.collection_name.createIndex({ key: 1 })  // 1 for ascending, -1 for descending
    ```

2. **List Indexes:**
    ```javascript
    db.collection_name.getIndexes()
    ```

## Aggregation Pipeline Stages

1. **Match Documents:**
    ```javascript
    { $match: { key: "value" } }
    ```

2. **Project Fields:**
    ```javascript
    { $project: { new_key: 1, _id: 0 } }
    ```

3. **Group Documents:**
    ```javascript
    { $group: { _id: "$key", total: { $sum: "$value" } } }
    ```

4. **Sort Documents:**
    ```javascript
    { $sort: { key: 1 } }  // 1 for ascending, -1 for descending
    ```

## Text Indexing and Search

1. **Text Index with Weights:**
    ```javascript
    db.collection_name.createIndex({ key1: "text", key2: "text" }, { weights: { key1: 3, key2: 1 } })
    ```

2. **Text Search with Score:**
    ```javascript
    db.collection_name.find(
      { $text: { $search: "search_term" } },
      { score: { $meta: "textScore" } }
    ).sort({ score: { $meta: "textScore" } })
    ```

## TTL Index (Time-To-Live)

1. **Create TTL Index:**
    ```javascript
    db.collection_name.createIndex({ created_at: 1 }, { expireAfterSeconds: 3600 })
    ```

## Change Streams

1. **Watch for Changes:**
    ```javascript
    const cursor = db.collection_name.watch()
    ```

2. **Listen to Change Events:**
    ```javascript
    while (!cursor.isExhausted()) {
      if (cursor.hasNext()) {
        const change = cursor.next()
        // Process change event
      }
    }
    ```

## Full-Text Search with `$text` Operator

1. **Create Text Index:**
    ```javascript
    db.collection_name.createIndex({ key: "text" })
    ```

2. **Perform Full-Text Search:**
    ```javascript
    db.collection_name.find({ $text: { $search: "search_term" } })
    ```

## Geospatial Queries

1. **Create 2D Index:**
    ```javascript
    db.collection_name.createIndex({ location: "2d" })
    ```

2. **Find Documents Near a Point:**
    ```javascript
    db.collection_name.find({ location: { $near: [x, y] } })
    ```

3. **Find Documents Within a Radius:**
    ```javascript
    db.collection_name.find({ location: { $geoWithin: { $centerSphere: [[x, y], radius] } } })
    ```

## Database Profiling

1. **Enable Profiling:**
    ```javascript
    db.setProfilingLevel(1, { slowms: 50 })
    ```

2. **View Profiling Data:**
    ```javascript
    db.system.profile.find().pretty()
    ```

## Sharding

1. **Enable Sharding for a Database:**
    ```javascript
    sh.enableSharding("database_name")
    ```

2. **Shard a Collection:**
    ```javascript
    sh.shardCollection("database_name.collection_name", { shard_key: 1 })
    ```
